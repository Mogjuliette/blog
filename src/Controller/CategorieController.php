<?php

namespace App\Controller;

use App\Entities\Categorie;
use App\Repository\CategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/categorie')]
class CategorieController extends AbstractController
{
    private CategorieRepository $repo;

    public function __construct(CategorieRepository $repo)
    {
        $this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all() {
        
        $categories = $this->repo->findAll();
        return $this->json($categories); 
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id){
        $categorie = $this->repo->findById($id);
        if(!$categorie){
            throw new NotFoundHttpException();
        }
        return $this->json($categorie);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer){ 
        
        $categorie = $serializer->deserialize($request->getContent(), Categorie::class, 'json');
    
        $this->repo->persist($categorie);

        return $this->json($categorie, Response::HTTP_CREATED);
    }
   
    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {   
        $categorie = $this->repo->findById($id);
        if(!$categorie){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), Categorie::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $categorie = $this->repo->findById($id);
        if(!$categorie){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($categorie);

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}