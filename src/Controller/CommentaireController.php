<?php

namespace App\Controller;
use App\Entities\Commentaire;
use App\Repository\CommentaireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/commentaire')]
class CommentaireController extends AbstractController{

    private CommentaireRepository $repo;

    public function __construct(CommentaireRepository $repo) {
    	$this->repo = $repo;
    }

    
    #[Route( methods: 'GET')]
    public function all() {
        
        $commentaire = $this->repo->findAll();
        return $this->json($commentaire); 

    }

    #[Route('/article/{id}', methods: 'GET')]
    public function allByArticle(int $id) {
        
        $commentaire = $this->repo->findByArticleId($id);
        return $this->json($commentaire); 

    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id){
        $commentaire = $this->repo->findById($id);
        if(!$commentaire){
            throw new NotFoundHttpException();
        }
        return $this->json($commentaire);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer){ 
        
        $commentaire = $serializer->deserialize($request->getContent(), Commentaire::class, 'json');
    
        $this->repo->persist($commentaire);

        return $this->json($commentaire, Response::HTTP_CREATED);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer) {   
        $commentaire = $this->repo->findById($id);
        if(!$commentaire){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), Commentaire::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);

        return $this->json($toUpdate);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $commentaire = $this->repo->findById($id);
        if(!$commentaire){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($commentaire);

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}