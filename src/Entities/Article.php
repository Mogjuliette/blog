<?php

namespace App\Entities;
use DateTime;

class Article{

    private ?int $id;
    private string $titre;
    private string $image;
    private string $auteurBD;
    private string $auteurArticle;
    private DateTime $date;
    private string $texte;
    private bool $serieEnCours;
	private string $genre;
    private int $vues;

    /**
     * @param string $titre
     * @param string $image
     * @param string $auteurBD
     * @param string $auteurArticle
     * @param DateTime $date
     * @param string $texte
     * @param bool $serieEnCours
	 * @param string $genre
     * @param int $vues
     * @param int|null $id
     */
    public function __construct( string $titre, string $image, string $auteurBD, string $auteurArticle, DateTime $date, string $texte, bool $serieEnCours, string $genre, int $vues, ?int $id = null) {
    	$this->id = $id;
    	$this->titre = $titre;
    	$this->image = $image;
    	$this->auteurBD = $auteurBD;
    	$this->auteurArticle = $auteurArticle;
    	$this->date = $date;
    	$this->texte = $texte;
    	$this->serieEnCours = $serieEnCours;
		$this->genre = $genre;
    	$this->vues = $vues;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTitre(): string {
		return $this->titre;
	}
	
	/**
	 * @param string $titre 
	 * @return self
	 */
	public function setTitre(string $titre): self {
		$this->titre = $titre;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getImage(): string {
		return $this->image;
	}
	
	/**
	 * @param string $image 
	 * @return self
	 */
	public function setImage(string $image): self {
		$this->image = $image;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getAuteurBD(): string {
		return $this->auteurBD;
	}
	
	/**
	 * @param string $auteurBD 
	 * @return self
	 */
	public function setAuteurBD(string $auteurBD): self {
		$this->auteurBD = $auteurBD;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getAuteurArticle(): string {
		return $this->auteurArticle;
	}
	
	/**
	 * @param string $auteurArticle 
	 * @return self
	 */
	public function setAuteurArticle(string $auteurArticle): self {
		$this->auteurArticle = $auteurArticle;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getDate(): DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime $date 
	 * @return self
	 */
	public function setDate(DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTexte(): string {
		return $this->texte;
	}
	
	/**
	 * @param string $texte 
	 * @return self
	 */
	public function setTexte(string $texte): self {
		$this->texte = $texte;
		return $this;
	}
	
	/**
	 * @return bool
	 */
	public function getSerieEnCours(): bool {
		return $this->serieEnCours;
	}
	
	/**
	 * @param bool $serieEnCours 
	 * @return self
	 */
	public function setSerieEnCours(bool $serieEnCours): self {
		$this->serieEnCours = $serieEnCours;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getGenre(): string {
		return $this->genre;
	}
	
	/**
	 * @param string $genre 
	 * @return self
	 */
	public function setGenre(string $genre): self {
		$this->genre = $genre;
		return $this;
	}
	/**
	 * @return int
	 */
	public function getVues(): int {
		return $this->vues;
	}
	
	/**
	 * @param int $vues 
	 * @return self
	 */
	public function setVues(int $vues): self {
		$this->vues = $vues;
		return $this;
	}

	

	
}