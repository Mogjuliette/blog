<?php

namespace App\Entities;

class Categorie {
    private ?int $id;
    private string $nom;


    /**
     * @param int|null $id
     * @param string $nom
     */
    public function __construct(string $nom, ?int $id) {
    	$this->id = $id;
    	$this->nom = $nom;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getNom(): string {
		return $this->nom;
	}
	
	/**
	 * @param string $nom 
	 * @return self
	 */
	public function setNom(string $nom): self {
		$this->nom = $nom;
		return $this;
	}
}