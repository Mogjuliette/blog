<?php

namespace App\Entities;
use DateTime;

class Commentaire {

    private ?int $id;
    private string $auteur;
    private string $texteCom;
    private DateTime $date;


    /**
     * @param string $auteur
     * @param string $texte
     * @param DateTime $date
     * @param int|null $id
     */
    public function __construct(string $auteur, string $texteCom, DateTime $date, ?int $id) {
    	$this->id = $id;
    	$this->auteur = $auteur;
    	$this->texteCom = $texteCom;
    	$this->date = $date;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getAuteur(): string {
		return $this->auteur;
	}
	
	/**
	 * @param string $auteur 
	 * @return self
	 */
	public function setAuteur(string $auteur): self {
		$this->auteur = $auteur;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTexteCom(): string {
		return $this->texteCom;
	}
	
	/**
	 * @param string $texte 
	 * @return self
	 */
	public function setTexte(string $texteCom): self {
		$this->texteCom = $texteCom;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getDate(): DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime $date 
	 * @return self
	 */
	public function setDate(DateTime $date): self {
		$this->date = $date;
		return $this;
	}
}