<?php

namespace App\Repository;

use App\Entities\Article;
use App\Entities\Categorie;
use DateTime;
use PDO;

class ArticleRepository {

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    private function sqlToArticle(array $line):Article {
        return new Article($line['titre'], $line['image'], $line['auteurBD'], $line['auteurArticle'], new DateTime($line['date']), $line['texte'], $line['serieEnCours'], $line['genre'], $line['vues'], $line['id']);
    }


    public function findAll():array{

        $array = [];

        $statement = $this->connection->prepare('SELECT * FROM article');

        $statement->execute();

        $results = $statement->fetchAll();
        
        foreach ($results as $line) {
                $array[] = $this->sqlToArticle($line);
            }
        return $array;
    }

    public function findById(int $id):Article|null{
        $statement = $this->connection->prepare('SELECT * FROM article WHERE id = :id');
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();
        if($result){
            $article = $this->sqlToArticle($result);
        return $article;
        }
        return null;
    }

    public function persist(Article $article):void {
        $statement = $this->connection->prepare('INSERT INTO article (titre, image, auteurBD, auteurArticle, date, texte, serieEnCours, genre, vues) VALUES (:titre, :image, :auteurBD, :auteurArticle, :date, :texte, :serieEnCours, :genre, :vues)');
        $statement->bindValue('titre', $article->getTitre(), PDO::PARAM_STR);
        $statement->bindValue('image', $article->getImage(), PDO::PARAM_STR);
        $statement->bindValue('auteurBD', $article->getAuteurBD(), PDO::PARAM_STR);
        $statement->bindValue('auteurArticle', $article->getAuteurArticle(), PDO::PARAM_STR);
        $statement->bindValue('date', $article->getDate()->format("Y-m-d"), PDO::PARAM_STR);
        $statement->bindValue('texte', $article->getTexte(), PDO::PARAM_STR);
        $statement->bindValue('serieEnCours', $article->getSerieEnCours(), PDO::PARAM_BOOL);
        $statement->bindValue('genre', $article->getgenre(), PDO::PARAM_STR);
        $statement->bindValue('vues', $article->getVues(), PDO::PARAM_INT);

        $statement->execute();

        $article->setId($this->connection->lastInsertId());

    }

    public function update(Article $article) {
        $statement = $this->connection->prepare('UPDATE article SET titre=:titre, image=:image, auteurBD=:auteurBD, auteurArticle=:auteurArticle, date=:date, texte=:texte, serieEnCours=:serieEnCours, vues=:vues WHERE id=:id');
        $statement->bindValue('titre', $article->getTitre(), PDO::PARAM_STR);
        $statement->bindValue('image', $article->getImage(), PDO::PARAM_STR);
        $statement->bindValue('auteurBD', $article->getAuteurBD(), PDO::PARAM_STR);
        $statement->bindValue('auteurArticle', $article->getAuteurArticle(), PDO::PARAM_STR);
        $statement->bindValue('date', $article->getDate()->format("Y-m-d"), PDO::PARAM_STR);
        $statement->bindValue('texte', $article->getTexte(), PDO::PARAM_STR);
        $statement->bindValue('serieEnCours', $article->getSerieEnCours(), PDO::PARAM_BOOL);
        $statement->bindValue('vues', $article->getVues(), PDO::PARAM_INT);
        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    public function delete(Article $article) {
        $statement = $this->connection->prepare('DELETE FROM article WHERE id=:id');
        $statement->bindValue('id', $article->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    
}