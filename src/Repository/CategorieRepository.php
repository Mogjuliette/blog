<?php

namespace App\Repository;

use App\Entities\Categorie;
use DateTime;
use PDO;

class CategorieRepository{
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    private function sqlToCategorie(array $line):Categorie {
        return new Categorie($line['nom'], $line['id']);
    }

    public function findAll():array{

        $array = [];

        $statement = $this->connection->prepare('SELECT * FROM categorie');

        $statement->execute();

        $results = $statement->fetchAll();
        if($results){
        foreach ($results as $line) {
            $array[] = $this->sqlToCategorie($line);
        }
        }
        return $array;
    }

    public function findById(int $id):Categorie|null{
        $statement = $this->connection->prepare('SELECT * FROM categorie WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $result = $statement->fetch();
        if($result){
        return $this->sqlToCategorie($result);
        }
        return null;
    }

    public function persist(Categorie $categorie):void {
        $statement = $this->connection->prepare('INSERT INTO categorie (nom) VALUES (:nom)');
        $statement->bindValue('nom', $categorie->getNom(), PDO::PARAM_STR);

        $statement->execute();

        $categorie->setId($this->connection->lastInsertId());
    }

    public function update(Categorie $categorie) {
        $statement = $this->connection->prepare('UPDATE categorie SET nom=:nom WHERE id=:id');
        $statement->bindValue('nom', $categorie->getnom(), PDO::PARAM_STR);
        $statement->bindValue('id', $categorie->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    public function delete(Categorie $categorie) {
        $statement = $this->connection->prepare('DELETE FROM categorie WHERE id=:id');
        $statement->bindValue('id', $categorie->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    public function findByArticle(int $id):array{

        $array = [];

        $statement = $this->connection->prepare('SELECT * FROM categorie LEFT JOIN article_categorie WHERE :id = article_categorie.id_article');

        $statement->bindValue('id', $id, PDO::PARAM_INT);


        $statement->execute();

        $results = $statement->fetchAll();
        if($results){
        foreach ($results as $line) {
            $array[] = $this->sqlToCategorie($line);
        }
        }
        return $array;
    }
    
}