<?php

namespace App\Repository;

use App\Entities\Commentaire;
use DateTime;
use PDO;

class CommentaireRepository {

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    private function sqlToCommentaire(array $line):Commentaire {
        return new Commentaire($line['auteur'], $line['texteCom'], new DateTime($line['date']), $line['comId']);

    }


    public function findAll():array{

        $array = [];

        $statement = $this->connection->prepare('SELECT *, commentaire.id comId FROM commentaire');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $line) {
            $comment = $this->sqlToCommentaire($line);
            $array[] = $comment;
        }
        return $array;
    }

    public function findByArticleId(int $id):array{

        $array = [];

        $statement = $this->connection->prepare('SELECT *, commentaire.id comId FROM commentaire WHERE id_article = :id');
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $line) {
            $comment = $this->sqlToCommentaire($line);
            $array[] = $comment;
        }
        return $array;
    }

    public function findById(int $id):Commentaire|null{
        $statement = $this->connection->prepare('SELECT *, commentaire.id comId FROM commentaire WHERE commentaire.id = :id');
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetch();
        $line = $result;
        if($line){
            $comment = $this->sqlToCommentaire($line);
            return $comment;
            }
        return null;
    }

    public function persist(Commentaire $commentaire, int $id_article):void {
        $statement = $this->connection->prepare('INSERT INTO commentaire (auteur, texteCom, date, id_article) VALUES (:auteur, :texteCom, :date, :id_article)');
        $statement->bindValue('auteur', $commentaire->getAuteur(), PDO::PARAM_STR);
        $statement->bindValue('texteCom', $commentaire->getTexteCom(), PDO::PARAM_STR);
        $statement->bindValue('date', $commentaire->getDate()->format("Y-m-d"), PDO::PARAM_STR);
        $statement->bindValue('id_article', $id_article, PDO::PARAM_INT);

        $statement->execute();

        $commentaire->setId($this->connection->lastInsertId());
    }
    public function update(Commentaire $commentaire, int $id_article):void {
        $statement = $this->connection->prepare('UPDATE commentaire SET auteur=:auteur, texteCom=:texteCom, date=:date, id_article=:id_article WHERE id=:id');
        $statement->bindValue('auteur', $commentaire->getAuteur(), PDO::PARAM_STR);
        $statement->bindValue('texteCom', $commentaire->getTexteCom(), PDO::PARAM_STR);
        $statement->bindValue('date', $commentaire->getDate()->format("Y-m-d"), PDO::PARAM_STR);
        $statement->bindValue('id_article', $id_article, PDO::PARAM_INT);
        $statement->bindValue('id', $commentaire->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    public function delete(Commentaire $commentaire) {
        $statement = $this->connection->prepare('DELETE FROM commentaire WHERE id=:id');
        $statement->bindValue('id', $commentaire->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

}