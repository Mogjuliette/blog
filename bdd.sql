-- Active: 1674203942125@@127.0.0.1@3306@blog

DROP TABLE IF EXISTS article_categorie;
DROP TABLE IF EXISTS commentaire;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS categorie;

CREATE TABLE categorie(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(255)
    );

CREATE TABLE article(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    titre VARCHAR(255),
    image VARCHAR(255),
    auteurBD VARCHAR(255),
    auteurArticle VARCHAR(255),
    date DATE,
    texte VARCHAR(30000),
    serieEnCours BOOLEAN,
    genre VARCHAR(255),
    vues INT
);

CREATE TABLE article_categorie(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    id_article int,
    id_categorie int,    
    Foreign Key (id_article) REFERENCES article(id) ON DELETE CASCADE,
    Foreign Key (id_categorie) REFERENCES categorie(id)ON DELETE CASCADE
);

CREATE TABLE commentaire(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    auteur VARCHAR(255),
    texteCom VARCHAR(3000),
    date DATETIME,
    id_article int,
    Foreign Key (id_article) REFERENCES article(id) ON DELETE CASCADE
);

INSERT INTO categorie (nom) VALUES ("Romance"),("Aventure"),("Tranche de vie"),("Représentation LGBT"),("Traduction en français disponible"),("Disponible en format papier"),("Francophone"),("Fantasy"),("Science Fiction"),("Wholesome");

INSERT INTO article (titre, image, auteurBD, auteurArticle, date, texte, serieEnCours, genre, vues) VALUES ("Cat's cafe", "https://i.etsystatic.com/isbl/f3c541/38731159/isbl_1680x420.38731159_qhrm0b9z.jpg?version=1", "Matt Tarpley", "Juliette Suc", "2023-02-09", "Cat's cafe est l'un de mes webcomics préférés, et de loin ! Il réussit à produire un mélange d'humour, de mignon, et de bien-être très réussi ! La santé mentale est un thème important de ce comic. 
Suivez la vie de Cat, qui tient un café à l'aide de Rabbit, un petit lapin anxieux, et de leurs habitués, comme Penguin, l'invétéré amateur de café !", 1, "Wholesome", 2),("Space Boy", "https://static.tvtropes.org/pmwiki/pub/images/ooooooooo_4.jpg", " Stephen McCranie's", "Juliette Suc", "2023-02-09", "Space Boy est aussi l'un de mes favoris. Je ne sais pas comment décrire la série à part dire qu'elle est vraiment très bien. Les personnages sont bien écrits, on ressent vraiment des émotions en le lisant. 
Amy, une jeune fille qui associe chaque personne à une saveur, vit dans une station minière dans l'espace. A la suite d'un incident, sa famille doit partir vivre sur Terre, 30 ans dans le futur, le temps que leurs corps cryogénisés fassent le voyage. Su place, elle rencontre un étrange garçon, Oliver.", 1, "Science-fiction", 3),("Always Human", "https://i0.wp.com/paulsemel.com/wp-content/uploads/2020/05/Ari-North-Always-Human-main-dropbox.jpg?fit=800%2C451&ssl=1", "Ari North", "Juliette Suc", "2023-02-09", "Always Human, encore une de mes séries préférées, avec à la fois une histoire très bien écrite et des dessins avec une atmosphère fantastique. Les enjeux de l'histoire ne sont finalement pas très élevés, ici pas question de sauver le monde, mais plutôt de savoir comment aborder les relations humaines. Il y a une très bonne diversité des représentations, à la fois LGBT, handicapés, ou raciales. 
Dans le futur, Sunati, change d'apparence régulièrement. Chacun peut utiliser des mods pour choisir sa couleur d'yeux, de cheveux, et encore beaucoup d'autres choses. Mais elle est intriguée par Austin, une jeune fille qui ne semble pas utiliser de mods du tout.", 0, "Romance", 1),("Gourmet Hound","https://swebtoon-phinf.pstatic.net/20171025_252/1508878599647EKo88_JPEG/thumbnail.jpg","Leehama","Juliette Suc","2023-02-13","Lucy, a woman with an uncanny sense of taste and smell, discovers that her favorite restaurant has changed kitchen staff--and she does not know the identity of the chef whose cooking she's loved for years. When a lucky accident leads her to two former chefs at Dimanche, she decides that she will do her utmost to track down each of their old colleagues in order to rediscover that perfect taste.", 0, "Romance", 0),("Muted","https://pbs.twimg.com/media/D-MIGS9UwAAClWp.jpg","Miranda Mundt","Juliette Suc","2023-02-13","On the full moon of her 21st year, the young witch, Camille Severin, is expected to perform the traditional ritual to summon forth a winged demon for her families success and prosperity. But when the ritual goes wrong, it reveals the terrifying truths about herself and the secrets that threaten to tear her family apart. Aliquam venenatis efficitur enim eu ornare. Curabitur dictum in tellus sed rhoncus. Curabitur gravida diam odio, et accumsan ipsum finibus sit amet. Suspendisse magna diam, blandit a erat ut, bibendum feugiat metus. Mauris pellentesque leo nec velit efficitur eleifend. Donec in massa ligula. Vivamus eros nulla, dictum id justo vel, elementum laoreet odio. Ut ultricies finibus neque nec viverra. Proin sit amet leo convallis, accumsan nulla vel, maximus ante. Nunc iaculis justo est, quis pharetra velit luctus elementum. Integer vulputate mi eu eros accumsan, vel varius felis pretium. Maecenas in est congue, elementum est eu, sodales orci. Cras sollicitudin vel mauris vitae pulvinar. Aliquam dapibus at dui et efficitur. Quisque fringilla arcu non faucibus rutrum. Vestibulum pulvinar dignissim sollicitudin.", 0, "Drame", 0),("Le Château d'Ambre","https://www.parismanga.fr/wp-content/uploads/2022/02/Art-of-K-500x500-1.jpg","Art of K","Juliette Suc","2023-02-26","Curabitur non sollicitudin eros. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur consequat felis ac elit sagittis, sit amet viverra diam maximus. Sed porta blandit turpis nec rhoncus. Suspendisse viverra massa sit amet ex rhoncus, ac tincidunt augue fermentum. Phasellus et commodo quam, eu porta dui. Donec sed tempus risus, id condimentum metus. Fusce malesuada, lorem pretium gravida consectetur, massa leo scelerisque tortor, a euismod metus felis non orci. Maecenas laoreet facilisis nisl a accumsan. Ut pretium a nisl ac faucibus. Donec sit amet urna scelerisque, lobortis nisi non, placerat eros. Praesent lobortis sollicitudin commodo. In dictum congue bibendum. Morbi commodo dui ac porttitor laoreet. Maecenas gravida imperdiet nulla, eget auctor orci bibendum in.", 1, "Policier", 2),("Stand Still Stay Silent","http://www.comicstories.fr/wp-content/uploads/2020/02/Stand-Still-Stay-Silent-1-e1581525449791.jpg","Minna Sundberg","Juliette Suc","2023-02-26","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi elementum nibh in eros laoreet euismod. Proin venenatis nibh nec magna gravida, vitae suscipit velit vulputate. Ut nulla massa, efficitur at commodo a, accumsan non libero. In hac habitasse platea dictumst. Vivamus dictum tempus enim in porta. Suspendisse potenti. In eu interdum magna. Nullam congue metus vel semper tempor. Quisque dignissim pellentesque elit ac pellentesque. Ut mollis tellus ut tellus pharetra, in accumsan nisl venenatis. Nulla non varius nulla, eu facilisis orci. Donec scelerisque est vel mauris finibus vulputate. Donec rhoncus orci eu eros porttitor facilisis. Phasellus at feugiat ante. Nulla iaculis turpis in justo suscipit dignissim.

Duis vel purus vulputate, euismod erat ut, maximus velit. Nulla nisl massa, venenatis eget turpis ac, malesuada consectetur massa. Pellentesque id neque quis justo consectetur laoreet. Duis quis luctus dolor. Etiam sit amet dignissim metus. Praesent venenatis odio lorem, ut ultricies leo maximus sit amet. Duis id diam at ligula luctus molestie.", 1, "Post-apo", 4);

INSERT INTO article_categorie (id_article, id_categorie) VALUES (1,3),(1,6),(1,10),(2,1),(2,2),(2,3),(2,5),(2,6),(2,9),(3,1),(3,3),(3,4),(3,6),(3,9);

INSERT INTO commentaire (auteur, texteCom, date, id_article) VALUES ("Eliranel", "J'ai adoré cette série !", "2023-02-10", 2),("Dyna", "Cat's cafe c'est vraiment trop trop bien !", "2023-02-10", 1),("Anonyme", "Très bon webtoon !", "2023-02-11", 3),("Fae", "Je kiffe trop cette série !", "2023-02-25", 2),("Vorpaline", "J'ai adoré Muted ! Par contre les thèmes abordés sont difficiles", "2023-02-25", 5);

/* SELECT * FROM article LEFT JOIN commentaire ON article.id = commentaire.id_article;

SELECT *, commentaire.id comId FROM commentaire LEFT JOIN article ON article.id = commentaire.id_article WHERE commentaire.id = :id */

/* SELECT * FROM article_categorie ac LEFT JOIN article ON article.id = ac.id_article LEFT JOIN categorie ON categorie.id = ac.id_categorie ORDER BY article.id;

SELECT *, categorie.id catId, article.id artId FROM article_categorie ac LEFT JOIN article ON article.id = ac.id_article LEFT JOIN categorie ON categorie.id = ac.id_categorie WHERE article.id = :id;

INSERT INTO article_categorie (id_article, id_categorie) VALUES (1,4);
 */
