# Projet Blog

Ce blog est centré autour des reviews de webcomics, des bandes-dessinées disponibles en ligne. Chaque article parle d'une série en particulier, et donne un certain nombre d'informations dessus (auteur, est-ce que la série est terminée ou non, quel est son genre, etc). 

J'ai voulu partir sur du many to many pour les catégories des articles, puisqu'une série se situe souvent à la croisée de plusieurs genres différents. Mais j'ai manqué de temps pour résoudre les problèmes techniques liés à tout ça, et j'ai défait ce que j'avais fait pour revenir à une colonne "genre" toute simple dans la classe article. J'ai quand même fait des commentaires en one to many pour montrer que je pouvais gérer des relations entre classes.

Les fonctionnalités attendues sont de pouvoir consulter la liste de tout les articles, les articles individuellement et de pouvoir modifier, supprimer ou ajouter un article, sans gestion des users. J'ai fait toutes ces étapes, en peinant un bon moment sur les formulaires. En effet, j'avais une date enregistrée comme date en sql mais que j'ai repassé en string en typescript puisque c'était trop compliqué à gérer. Malgré tout, il fallait gérer la conversion en coupant des bouts de date en ts pour les passer dans un format JSON pour le back. En plus de ça, j'ai mis un booléen pour savoir si la série est en cours ou non, que je voulais choisir via des radio checks. J'ai dû chercher comment leur faire passer une valeur booléenne dans le submit, puisque je ne pouvais pas juste l'indiquer dans value={} en tant que booléen. 

J'ai aussi essayé de faire en sorte que la date proposé soit celle du jour d'aujourd'hui dans le questionnaire de création d'article et soit celle de la date de la première publication dans le formulaire pour modifier un article.

J'ai essayé de mettre des petites fonctionnalités, comme un modal qui demande la confirmation avant la suppression d'un article lorsqu'on veut le supprimer. Le reste du design est très basique, j'ai principalement exploité bootstrap (et profité de son système de col pour faire du responsive). J'aurais aimé avoir le temps d'écrire des vrais articles, je le ferai peut-être à l'avenir.

Dans les fonctionnalités que j'aurais aimé ajouter, à part le many to many, c'est principalement la barre de recherche, et la gestion des commentaires (le back le fait, mais pas encore le front).

Les maquettes sont disponibles sur ce lien : https://www.figma.com/file/Tb8wfYjS1jQwolOHEZbLJS/Blog?node-id=0%3A1&t=HrWANETDeIJliKh5-1
Tout à beaucoup changé donc il faudrait que je les reprenne.




## Projet Blog Consignes

L'objectif du projet est de créer une petite application de blog sans authentification en utilisant Symfony pour le backend et React pour le frontend.

## Fonctionnalités attendues
* Créer des articles
* Consulter la liste des articles
* Consulter un article spécifique
* Modifier ou supprimer un article

La gestion des users n'est pas attendue, on va considérer que tout le monde peut poster un article en mettant juste son nom dans le formulaire.

### Fonctionalités bonus 
* Ajouter une barre de recherche pour les articles
* Permettre des commentaires sur les posts
* Ajouter des catégories pour les articles
* Ajouter un compteur de vue sur les posts

## Travail attendu
* Créer les wireframes des différentes pages de l'application (mobile first)
* Créer le script de mise en place de la base de données et les composants d'accès aux données pour la ou les tables avec PDO (repository)
* Créer une API Rest avec Symfony (les contrôleurs et la validation)
* Créer le front (**responsive**) de l'application avec React/Next
* Requêter l'API Rest depuis des services de typescript utilisés dans les components React

